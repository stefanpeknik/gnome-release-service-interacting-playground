import requests
import argparse
import sys
import tempfile
from pprint import pprint
from urllib.parse import urljoin, urlparse


def download_file_from_url(url, to):
    response = requests.get(url)
    response.raise_for_status()
    to.write(response.content)


def send_tarball(url, jwt, tarball_uri):
    install_url = urljoin(url, "install-module-release")
    with tempfile.TemporaryDirectory() as tempdir:
        filename = urlparse(tarball_uri).path.split("/")[-1]
        with open(f"{tempdir}/{filename}", "wb") as file:
            download_file_from_url(tarball_uri, to=file)
        with open(f"{tempdir}/{filename}", "rb") as file:
            files = {"tarball": file}
            headers = {"Authorization": f"Bearer {jwt}"}
            res = requests.post(install_url, files=files, headers=headers)
            return res


def release_suites(url, jwt, versions, new_version):
    release_url = url + "release-suites"
    with tempfile.NamedTemporaryFile() as file:
        if isinstance(versions, str):
            versions = versions.encode("utf-8")
        else:
            file.write(versions)
        files = {"versions_file": file}
        data = {
            "new_version": new_version,
        }
        headers = {"Authorization": f"Bearer {jwt}"}
        res = requests.post(release_url, files=files, data=data, headers=headers)
        return res


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest="command")
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument("--jwt", required=True, help="JWT token.")
    parent_parser.add_argument(
        "--release-api", required=True, help="GNOME Release Service API URL."
    )
    # ================== module ==================
    module_parser_name = "release-module"
    module_parser = subparsers.add_parser(
        module_parser_name, parents=[parent_parser], help="Install a module release."
    )

    module_parser.add_argument(
        "-t", "--tarball", required=True, help="URL path of the tarball to install."
    )
    # ===========================================
    # ================= suites ==================
    suites_parser_name = "release-suites"
    suites_parser = subparsers.add_parser(
        suites_parser_name, parents=[parent_parser], help="Release suites."
    )
    suites_parser.add_argument(
        "-n", "--new", required=True, help="Version of the release of suites."
    )
    suites_parser.add_argument(
        "versions",
        help="Text in format: one or more lines of suite:module:version:subdir (subdir is obsolete atm), where suite is the name of the suite, module is the name of the module to be included in that suite release, and version is the specific version of the module.",
    )
    # ===========================================

    args = parser.parse_args()

    if args.command is None:
        parser.print_help()
        sys.exit(1)

    if args.command == module_parser_name:
        response = send_tarball(args.release_api, args.jwt, args.tarball)
    elif args.command == suites_parser_name:
        response = release_suites(args.release_api, args.jwt, args.versions, args.new)

    print(response.status_code)
    pprint(response.json())
