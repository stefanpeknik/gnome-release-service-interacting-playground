# GNOME Release Service Playground

GNOME Release Service requires authentication by JWT tokens generated from GNOME Gitlab instance. 

The API is currently deployed in GNOME's OpenShift and is accessible on route: https://gnome-release-service-api.apps.openshift4.gnome.org.

This repository was created for the purpose of being able to interact with the API in its testing phase
by using Gitlab CI that generates such JWT tokens which other scripts 
(such as [the current small python script to interact with the API](./interact_with_api.py)) can use.

In order to see and validate the results of running the CI jobs that reach out to the API 
without having to dig in logs of the pod with the deployed API, a seperate route was created to display 
the current state of the wannabe FTP file system (mimics the old file system used by https://download.gnome.org/): 
https://gnome-filesystem-service-api.apps.openshift4.gnome.org/.

Feel free to try out and reach out to me with any questions or found bugs.
